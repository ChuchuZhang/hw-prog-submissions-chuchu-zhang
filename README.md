## README ##

Repository for submitting CS160 Fall 2015 programming assignments. 

* Name: <FIRST NAME> <LAST NAME(S)>
* CalNetID: <ID>
* Email: <email>
* Class: <CS160/CS260A>
* Section: <101-108>
* Favorite Color: <#HEX>