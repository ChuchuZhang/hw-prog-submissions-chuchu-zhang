package com.example.chuchu.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    int animalKind1 = -1;
    int animalKind2 = -1;
    double[] toHuman = {1/2.0, 1/3.2, 1/3.64, 1/20.0, 1/1.78, 1/8.89, 1};
    double[] fromHuman = {2, 3.2, 3.64, 20, 1.78, 8.89, 1};
    EditText firstNumber;
    EditText secondNumer;
    TextView result;
    Button btnAdd;
    double numGet;
    double day;
    double parameter;
    String animal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //parameter = toHuman[animalKind1]*fromHuman[animalKind2];

        firstNumber = (EditText)findViewById(R.id.edit_message);
        secondNumer = (EditText)findViewById(R.id.edit_message2);
        btnAdd = (Button)findViewById(R.id.btn);
        result = (TextView)findViewById(R.id.txtResult);
        btnAdd.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                try {

                    numGet = Double.parseDouble(firstNumber.getText().toString());
                    day = Double.parseDouble(secondNumer.getText().toString());
                    day += numGet * 365.0;
                    if (animalKind1 == -1 || animalKind2 == -1) {
                        result.setText("You do not choose an animal yet!");
                    } else {
                        switch(animalKind2) {
                            case 0:
                                animal = "bear";
                                break;
                            case 1:
                                animal = "cat";
                                break;
                            case 2:
                                animal = "dog";
                                break;
                            case 3:
                                animal = "hamper";
                                break;
                            case 4:
                                animal = "hippopotamus";
                                break;
                            case 5:
                                animal = "kangaroo";
                                break;
                            case 6:
                                animal = "human";
                                break;
                        }
                        parameter = toHuman[animalKind1] * fromHuman[animalKind2];
                        day *= parameter;

                        result.setText("I am a "+ Integer.toString((int)(day/365.0)) + " years and "
                                +Integer.toString((int)day - ((int)(day/365.0))*365)
                                + " days old " + animal);
                    }
                } catch (NumberFormatException e) {
                    result.setText("You need to enter both numbers!");
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    /** Called when the user click the Send button
    public void sendMessage(View view) {

        Intent intent = new Intent(this, DisplayMessageActivity.class);
        EditText editText = (EditText) findViewById(R.id.edit_message);
        String message = editText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }*/
    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.radio_human:
                if (checked)
                    animalKind1 = 6;
                break;
            case R.id.radio_Cat:
                if (checked)
                    animalKind1 = 1;
                break;
            case R.id.radio_Dog:
                if (checked)
                    animalKind1 = 2;
                break;
            case R.id.radio_Hamster:
                if (checked)
                    animalKind1 = 3;
                break;
            case R.id.radio_Hippopotamus:
                if (checked)
                    animalKind1 = 4;
                break;
            case R.id.radio_Kangaroo:
                if (checked)
                    animalKind1 = 5;
                break;
            case R.id.radio_Bear:
                if (checked)
                    animalKind1 = 0;
                break;
        }
    }
    public void onRadioButtonClicked2(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radio_human2:
                if (checked)
                    animalKind2 = 6;
                break;
            case R.id.radio_Cat2:
                if (checked)
                    animalKind2 = 1;
                break;
            case R.id.radio_Dog2:
                if (checked)
                    animalKind2 = 2;
                break;
            case R.id.radio_Hamster2:
                if (checked)
                    animalKind2 = 3;
                break;
            case R.id.radio_Hippopotamus2:
                if (checked)
                    animalKind2 = 4;
                break;
            case R.id.radio_Kangaroo2:
                if (checked)
                    animalKind2 = 5;
                break;
            case R.id.radio_Bear2:
                if (checked)
                    animalKind2 = 0;
                break;
        }
    }
}
