package com.example.noon.cuckoo;



import android.os.AsyncTask;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.google.android.gms.common.api.Scope;

public class EarthquakeAsync extends AsyncTask<Void, Integer, Void> {
    private ScrollingActivity activity;
    private EarthquakeList earthQlist;

    public EarthquakeAsync(ScrollingActivity acti) {
        this.activity = acti;
    }
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onProgressUpdate(Integer... values){
        super.onProgressUpdate(values);

    }

    @Override
    protected Void doInBackground(Void... arg0) {

        earthQlist = new EarthquakeList();
        earthQlist.loadFromUSGS();

        activity.addOutput(earthQlist);
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        //((ProgressBar)activity.findViewById(R.id.progress)).setVisibility(RelativeLayout.INVISIBLE);
    }
}