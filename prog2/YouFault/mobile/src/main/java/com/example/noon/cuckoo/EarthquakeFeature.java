package com.example.noon.cuckoo;

import android.util.Log;

import co.scarletshark.geojson.JsonObject;
import co.scarletshark.geojson.JsonParser;
import co.scarletshark.geojson.JsonValue;

import java.text.DateFormat;
import java.util.Date;


public class EarthquakeFeature {
    protected float mag;
    protected String place;
    protected long time;
    protected double updated;
    protected float tz;
    protected String url;
    protected String detail;
    protected float felt;
    protected float cdi;
    protected float mmi;
    protected String alert;
    protected String status;
    protected float tsunami;
    protected float sig;
    protected String net;
    protected String code;
    protected String ids;
    protected String sources;
    protected String types;
    protected float nst;
    protected float dmin;
    protected float rms;
    protected float gap;
    protected String magType;
    protected String type;
    protected EarthquakeFeatureGeometry geom;
    protected String title;

    public EarthquakeFeature(String json) {
        JsonObject feature = JsonParser.parseObject(json);
        JsonObject prop = feature.getPairByName("properties").getValueAsObject();

        geom = new EarthquakeFeatureGeometry(feature.getPairByName("geometry"));

        this.alert = prop.getPairByName("alert").getValue().toString();
        this.cdi = avoidNullPointerForNullValue(prop.getPairByName("cdi").getValue().toString());
        this.code = prop.getPairByName("code").getValue().toString();
        this.detail = prop.getPairByName("detail").getValue().toString();
        this.dmin = avoidNullPointerForNullValue(prop.getPairByName("dmin").getValue().toString());
        this.felt = avoidNullPointerForNullValue(prop.getPairByName("felt").getValue().toString());
        this.gap = avoidNullPointerForNullValue(prop.getPairByName("gap").getValue().toString());
        this.ids = prop.getPairByName("ids").getValue().toString();
        this.mag =avoidNullPointerForNullValue(prop.getPairByName("mag").getValue().toString());
        this.magType = prop.getPairByName("magType").getValue().toString();
        this.mmi = avoidNullPointerForNullValue(prop.getPairByName("mmi").getValue().toString());
        this.net = prop.getPairByName("net").getValue().toString();
        this.nst = avoidNullPointerForNullValue(prop.getPairByName("nst").getValue().toString());
        this.place = prop.getPairByName("place").getValue().toString();
        this.rms = avoidNullPointerForNullValue(prop.getPairByName("rms").getValue().toString());
        this.sig = avoidNullPointerForNullValue(prop.getPairByName("sig").getValue().toString());
        this.sources = prop.getPairByName("sources").getValue().toString();
        this.status = prop.getPairByName("status").getValue().toString();
        this.time = avoidNullPointerForNullValueL(prop.getPairByName("time").getValue());
        this.tsunami = avoidNullPointerForNullValue(prop.getPairByName("tsunami").getValue().toString());
        this.type = prop.getPairByName("type").getValue().toString();
        this.types = prop.getPairByName("types").getValue().toString();
        this.tz = avoidNullPointerForNullValue(prop.getPairByName("tz").getValue().toString());
        this.updated = avoidNullPointerForNullValueD(prop.getPairByName("updated").getValue().toString());
        this.url = prop.getPairByName("url").getValue().toString();
        this.title = prop.getPairByName("title").getValueAsString();
    }

    public static float avoidNullPointerForNullValue(String val) {
        if(val != "null") {
            return Float.parseFloat(val);
        } else {
            return 0;
        }
    }

    public static double avoidNullPointerForNullValueD(String val) {
        if(val != "null") {
            return Double.parseDouble(val);
        } else {
            return 0;
        }
    }

    public static long avoidNullPointerForNullValueL(JsonValue val) {
        return (long)val.getDoubleValue();
    }

    public float getMag() {
        return mag;
    }

    public void setMag(float mag) {
        this.mag = mag;
    }

    public String getPlace() {
        return place;
    }


    public long getTime() {
        return time;
    }

    @Override
    public String toString() {
        return "EarthquakeFeature{" +
                "mag=" + mag +
                ", place='" + place + '\'' +
                ", time=" + time +
                ", updated=" + updated +
                ", tz=" + tz +
                ", url='" + url + '\'' +
                ", detail='" + detail + '\'' +
                ", felt=" + felt +
                ", cdi=" + cdi +
                ", mmi=" + mmi +
                ", alert='" + alert + '\'' +
                ", status='" + status + '\'' +
                ", tsunami=" + tsunami +
                ", sig=" + sig +
                ", net='" + net + '\'' +
                ", code='" + code + '\'' +
                ", ids='" + ids + '\'' +
                ", sources='" + sources + '\'' +
                ", types='" + types + '\'' +
                ", nst=" + nst +
                ", dmin=" + dmin +
                ", rms=" + rms +
                ", gap=" + gap +
                ", magType='" + magType + '\'' +
                ", type='" + type + '\'' +
                ", geom=" + geom.toString() +
                '}';
    }
}