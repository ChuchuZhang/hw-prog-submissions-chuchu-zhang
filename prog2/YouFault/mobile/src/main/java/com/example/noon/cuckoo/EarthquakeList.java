package com.example.noon.cuckoo;


import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
//import java.util.Hashtable;
import java.net.URL;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.Date;
import java.util.TimeZone;


import co.scarletshark.geojson.JsonObject;
import co.scarletshark.geojson.JsonParser;

/**
 * Created by mouton on 04.10.13.
 */
public class EarthquakeList {

    String textSource = "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/2.5_day.geojson";

    protected ArrayList<String> earthquakelist;
    protected ArrayList<Double> allLongitude;
    protected ArrayList<Double> allLatitude;
    protected long mostRecentEpochTime;
    protected String mostRecentHumanTime;
    protected double mostRecentlongitude;
    protected double mostRecentlatitude;
    protected String mostRecentPlace;
    protected float mostRecentMag;
    public EarthquakeList() {
        this.allLatitude = new ArrayList<Double>();
        this.allLongitude = new ArrayList<Double>();
        this.earthquakelist = new ArrayList<String>();
    }
    public EarthquakeList(int i) {
        earthquakelist = new ArrayList<String>();
        this.allLongitude = new ArrayList<Double>();
        this.earthquakelist = new ArrayList<String>();
        if (i == 1) {
            textSource = "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/2.5_hour.geojson";
        }
    }
    public void loadFromUSGS() {

        URL textUrl;
        try {
            textUrl = new URL(this.textSource);
            BufferedReader bufferReader = new BufferedReader(new InputStreamReader(textUrl.openStream()));


            StringBuilder builder = new StringBuilder();
            for (String line = null; (line = bufferReader.readLine()) != null;) {
                builder.append(line).append("\n");
            }
            bufferReader.close();
            String json = '[' + builder.toString() + ']';

            try {
                JsonObject result    = JsonParser.parseObject(json);
                Object[] features = result.getPairByName("features").getValueAsArray();
                for(int i = 0; i < features.length;i++) {
                    String feature = features[i].toString();
                    try {

                        EarthquakeFeature ef = new EarthquakeFeature(feature);
                        Date date = new Date(ef.time);
                        DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                        format.setTimeZone(TimeZone.getTimeZone("America/San_Francisco"));
                        String formattedTime = format.format(date);
                        if (i == 0 ) {
                            mostRecentMag = ef.mag;
                            mostRecentEpochTime = ef.time;
                            mostRecentHumanTime = formattedTime;
                            mostRecentlatitude = ef.geom.getLatitude();
                            mostRecentlongitude = ef.geom.getLongitude();
                            mostRecentPlace = ef.place;
                        }
                        this.allLongitude.add(ef.geom.getLongitude());
                        this.allLatitude.add(ef.geom.getLatitude());
                        this.earthquakelist.add(String.valueOf(ef.mag) + "M " + "at " + ef.place + " at " + formattedTime);
                    } catch(ArrayIndexOutOfBoundsException e) {
                        Log.e("No location","No location for this event: " + feature);
                    }
                }
            } catch(Exception e) {
                e.printStackTrace();
            }
        } catch(UnknownHostException e) {

            e.printStackTrace();
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /*public Hashtable<String, EarthquakeFeature> getEarthquakelist() {
        return earthquakelist;
    }*/
    public ArrayList<Double> getAllLongitude() {return allLongitude;}
    public ArrayList<Double> getAllLatitude() {return allLatitude; }
    public ArrayList<String> getEarthquakelist() {
        return earthquakelist;
    }
    public long getMostRecentEpochTime() {return mostRecentEpochTime;}
    public String getMostRecentHumanTime() {return  mostRecentHumanTime; }
    public double getMostRecentlongitude() {return mostRecentlongitude;}
    public double getMostRecentlatitude() {return mostRecentlatitude;}
    public String getMostRecentPlace() {return mostRecentPlace;}
    public float getMostRecentMag() {return mostRecentMag;}

}
