package com.example.noon.cuckoo;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapActivity extends FragmentActivity {

    private GoogleMap mMap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        //TextView view = (TextView)findViewById(R.id.txtResult);
        //view.setText(String.valueOf(b.getDouble("longitude")));
        setUpMapIfNeeded();
        Bundle b = getIntent().getExtras();
        String message = b.getString("details");
        if (message.equals("hello")) {
            message = "Earthquake is here!";
        }
        double targetLongitude = b.getDouble("longitude");
        double targetLatitude = b.getDouble("latitude");
        LatLng latLng = new LatLng(targetLatitude, targetLongitude);
        MarkerOptions options = new MarkerOptions()
                .position(latLng)
                .title(message);
        mMap.addMarker(options);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
        }
    }
    public void onClickBtnHome(View v) {
        Intent intent = new Intent(getBaseContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
    public void onClickBtnList(View v) {
        Intent intent = new Intent(getBaseContext(), ScrollingActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
    public void onClickBtnPhoto(View v) {
        return;
    }
}
