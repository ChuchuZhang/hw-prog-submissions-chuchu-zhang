package com.example.noon.cuckoo;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import java.util.ArrayList;

public class QueryService extends Service {
    final static String MY_ACTION = "MY_ACTION";
    private GoogleApiClient mApiClient;
    private static final String START_ACTIVITY = "/start_activity";
    private static final int INTERVAL = 60*1000;
    private static final int SECOND = 1000;
    private double currentLongitude;
    private double currentLatitude;
    private double mostRecentLongitude;
    private double mostRecentLatitude;
    private long mostRecentEpochTime;
    private String mostRecentPlace;
    private float mostRecentMag;

    @Override
    public void onCreate() {
        super.onCreate();

        /* Initialize the googleAPIClient for message passing */
        mApiClient = new GoogleApiClient.Builder( this )
                .addApi( Wearable.API )
                .addApi(LocationServices.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle connectionHint) {
                        //Successfully connected
                    }

                    @Override
                    public void onConnectionSuspended(int cause) {
                        //Connection was interrupted
                    }
                })
                .build();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Kick off new work to do
        currentLongitude = intent.getExtras().getDouble("longitude");
        currentLatitude = intent.getExtras().getDouble("latitude");
        QueryStartTimer();
        return START_STICKY;
    }
    public class MyThread extends Thread{
        @Override
        public void run() {
            EarthquakeList earthQlist;
            earthQlist = new EarthquakeList(1);
            earthQlist.loadFromUSGS();
            mostRecentEpochTime = earthQlist.getMostRecentEpochTime();
            if (System.currentTimeMillis() - mostRecentEpochTime < INTERVAL) {
                mostRecentMag = earthQlist.getMostRecentMag();
                mostRecentLongitude = earthQlist.getMostRecentlongitude();
                mostRecentLatitude = earthQlist.getMostRecentlatitude();
                mostRecentPlace = earthQlist.getMostRecentPlace();
                mApiClient.connect();
                String distance = String.valueOf(distance());
                String out = String.valueOf(mostRecentMag) + " M earthquake happens at " + String.valueOf(distance) + " km from here, in "+ mostRecentPlace
                        + " at" + earthQlist.getMostRecentHumanTime();
                String marker = mostRecentPlace;
                sendMessage(START_ACTIVITY, out);
                Intent intent2 = new Intent(getBaseContext(), MapActivity.class);
                intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                //you need to add this flag since you're starting a new activity from a service
                intent2.putExtra("longitude", mostRecentLongitude);
                intent2.putExtra("latitude", mostRecentLatitude);
                intent2.putExtra("details", marker);
                startActivity(intent2);
            }
            stopSelf();
        }

    }
    /* Calculate the distacance between the most recent epicenter and current location */
    private float distance() {
        Location crntLocation=new Location("crntlocation");
        crntLocation.setLatitude(currentLatitude);
        crntLocation.setLongitude(currentLongitude);
        Location newLocation=new Location("newlocation");
        newLocation.setLatitude(mostRecentLatitude);
        newLocation.setLongitude(mostRecentLongitude);
        return crntLocation.distanceTo(newLocation) / 1000;
    }
    private void QueryStartTimer() {
        CountDownTimer timer = new CountDownTimer(INTERVAL, SECOND) {
            public void onTick(long millisUntilFinished) { }
            public void onFinish() {
                MyThread myThread = new MyThread();
                myThread.start();
                QueryStartTimer();
            }
        };
        timer.start();

    }
    //How to send a message to the WatchListenerService
    private void sendMessage( final String path, final String text ) {
        new Thread( new Runnable() {
            @Override
            public void run() {
                NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes( mApiClient ).await();
                for(Node node : nodes.getNodes()) {
                    MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(
                            mApiClient, node.getId(), path, text.getBytes() ).await();
                }
            }
        }).start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mApiClient.disconnect();
    }

}
