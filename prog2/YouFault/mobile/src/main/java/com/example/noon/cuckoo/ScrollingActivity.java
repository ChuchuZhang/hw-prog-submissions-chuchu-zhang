package com.example.noon.cuckoo;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

public class ScrollingActivity extends Activity {
    ArrayList<String> earthquakeList;
    ArrayList<Button> buttons;
    ArrayList<Double> allLongitude;
    ArrayList<Double> allLatitude;

    Button e1, e2, e3, e4, e5, e6, e7;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);
        e1 = (Button)findViewById(R.id.earthquake1);
        e2 = (Button)findViewById(R.id.earthquake2);
        e3 = (Button)findViewById(R.id.earthquake3);
        e4 = (Button)findViewById(R.id.earthquake4);
        e5 = (Button)findViewById(R.id.earthquake5);
        e6 = (Button)findViewById(R.id.earthquake6);
        e7 = (Button)findViewById(R.id.earthquake7);
        buttons = new ArrayList<Button>();
        allLongitude = new ArrayList<Double>();
        allLatitude = new ArrayList<Double>();
        buttons.add(e1);
        buttons.add(e2);
        buttons.add(e3);
        buttons.add(e4);
        buttons.add(e5);
        buttons.add(e6);
        buttons.add(e7);
        EarthquakeAsync load = new EarthquakeAsync(this);

        load.execute();
    }
    protected void addOutput(EarthquakeList earthQlist) {
        allLatitude = earthQlist.getAllLatitude();
        allLongitude = earthQlist.getAllLongitude();
        earthquakeList = earthQlist.getEarthquakelist();
        Log.d("Scroll",String.valueOf(earthquakeList.size()));
        for (int i = 0; i < 7 && i < earthquakeList.size(); i += 1) {
            buttons.get(i).setText(earthquakeList.get(i));
        }

    }
    public void onClickEarthquake7(View v) {
        if (allLongitude != null && allLongitude.size() > 6) {
            double longitude = allLongitude.get(6);
            double latitude = allLatitude.get(6);
            Intent intent2 = new Intent(getBaseContext(), MapActivity.class);
            intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent2.putExtra("longitude", longitude);
            intent2.putExtra("latitude", latitude);
            intent2.putExtra("details", "hello");
            startActivity(intent2);
        }
    }
    public void onClickEarthquake6(View v) {
        if (allLongitude != null && allLongitude.size() > 5) {
            double longitude = allLongitude.get(5);
            double latitude = allLatitude.get(5);
            Intent intent2 = new Intent(getBaseContext(), MapActivity.class);
            intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent2.putExtra("longitude", longitude);
            intent2.putExtra("latitude", latitude);
            intent2.putExtra("details", "hello");
            startActivity(intent2);
        }
    }
    public void onClickEarthquake5(View v) {
        if (allLongitude != null && allLongitude.size() > 4) {
            double longitude = allLongitude.get(4);
            double latitude = allLatitude.get(4);
            Intent intent2 = new Intent(getBaseContext(), MapActivity.class);
            intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent2.putExtra("longitude", longitude);
            intent2.putExtra("latitude", latitude);
            intent2.putExtra("details", "hello");
            startActivity(intent2);
        }
    }
    public void onClickEarthquake4(View v) {
        if (allLongitude != null && allLongitude.size() > 3) {
            double longitude = allLongitude.get(3);
            double latitude = allLatitude.get(3);
            Intent intent2 = new Intent(getBaseContext(), MapActivity.class);
            intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent2.putExtra("longitude", longitude);
            intent2.putExtra("latitude", latitude);
            intent2.putExtra("details", "hello");
            startActivity(intent2);
        }
    }
    public void onClickEarthquake3(View v) {
        if (allLongitude != null && allLongitude.size() > 2) {
            double longitude = allLongitude.get(2);
            double latitude = allLatitude.get(2);
            Intent intent2 = new Intent(getBaseContext(), MapActivity.class);
            intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent2.putExtra("longitude", longitude);
            intent2.putExtra("latitude", latitude);
            intent2.putExtra("details", "hello");
            startActivity(intent2);
        }
    }
    public void onClickEarthquake2(View v) {
        if (allLongitude != null && allLongitude.size() > 1) {
            double longitude = allLongitude.get(1);
            double latitude = allLatitude.get(1);
            Intent intent2 = new Intent(getBaseContext(), MapActivity.class);
            intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent2.putExtra("longitude", longitude);
            intent2.putExtra("latitude", latitude);
            intent2.putExtra("details", "hello");
            startActivity(intent2);
        }
    }
    public void onClickEarthquake1(View v) {
        if (allLongitude != null && allLongitude.size() > 0) {
            double longitude = allLongitude.get(0);
            double latitude = allLatitude.get(0);
            Intent intent2 = new Intent(getBaseContext(), MapActivity.class);
            intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent2.putExtra("longitude", longitude);
            intent2.putExtra("latitude", latitude);
            intent2.putExtra("details", "hello");
            startActivity(intent2);
        }
    }
}
