package com.example.noon.cuckoo;

import android.content.Intent;
import android.os.Bundle;
import android.support.wearable.activity.WearableActivity;
import android.support.wearable.view.BoxInsetLayout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ClockActivity extends WearableActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hour);
        //first, let's extract that extra hour information we passed over
        Intent intent = getIntent();
        String hour = intent.getStringExtra("Hour"); //note this is case sensitive

        if (hour != null) {
            //Programmatically set the text to the actual hour.
            TextView time = (TextView) findViewById(R.id.time);
            time.setText(hour);
        }
    }
}
